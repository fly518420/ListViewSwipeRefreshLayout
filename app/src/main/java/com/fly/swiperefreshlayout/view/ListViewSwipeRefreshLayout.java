package com.fly.swiperefreshlayout.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.ListView;

import com.fly.swiperefreshlayout.exception.ErrorMethodException;

/**
 * Created with Android Studio.
 * Package_Name：com.fly.glide
 * Project_Name：GlideDemo
 * User：郭鹏飞
 * Date：2015/12/23
 * Email：love518420@foxmail.com
 * Description：
 */
public class ListViewSwipeRefreshLayout extends SwipeRefreshLayout {

    // 最小滑动距离
    private int mTouchSlop;

    // 操作的子view数据展示
    private ListView mListView;

    // 记录按下和抬起的位置
    private float downY, moveY;

    // 是否在加载数据
    private boolean mLoading;

    // 加载数据的监听
    private OnLoadListener mLoadListener;
    // 刷新监听
    private OnRefreshDealListener mRefreshDealListener;

    // 刷新监听标记
    private boolean refreshListenerFlag = false;

    private View root;
    private Context mContext;

    public ListViewSwipeRefreshLayout(Context context) {
        this(context, null);
    }

    public ListViewSwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();

        refreshListenerFlag = true;
        setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mLoading) { // 刷新的时候正在加载
                    setRefreshing(false); // 刷新完成
                    if (mRefreshDealListener != null) { // 回调通知正在加载
                        mRefreshDealListener.onLoading();
                    }
                } else { // 没有加载
                    if (mRefreshDealListener != null) { // 回调刷新
                        mRefreshDealListener.onRefresh();
                    }
                }
            }
        });
    }

    /**
     * 设置操作ListView
     *
     * @param listView ListView
     * @param resId    footerview布局id
     * @param rootId   footerview根布局下面的布局这样实现的目的解决footerview在不显示的时候占据空间
     */
    public void setListView(@NonNull ListView listView,
                            int resId, int rootId) {
        mListView = listView;
        // 初始化加载布局
        View mFooterView = LayoutInflater.from(mContext).
                inflate(resId, mListView, false);
        root = mFooterView.findViewById(rootId);
        root.setVisibility(GONE); // 默认不显示加载布局
        setLoadingView(mFooterView);
    }

    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downY = ev.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
                moveY = ev.getRawY();
                if (!isRefreshing() && canLoadMore()) { // 双重条件判断 防止刷新的时候满足加载条件不断回调
                    load();
                }
                break;
            case MotionEvent.ACTION_UP:
                if (canLoadMore()) { // 这步的处理主要是实现刷新的时候启动加载，提示回调在一次操作进行一次
                    load();
                }
                break;
            default:
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 加载
     */
    private void load() {
        if (isRefreshing()) { // 正在刷新
            if (mLoadListener != null) { // 判断是否有加载回调
                mLoadListener.onRefreshing();
            }
        } else {
            if (mLoading) { // 因为加载实在 move 的处理 所以防止不断回调
                return;
            }
            mLoading = true; // 修改加载标记
            root.setVisibility(VISIBLE); // 显示加载布局
            mListView.setSelection(mListView.getCount() - 1); // 定位到最后一个子view的位置
            if (mLoadListener != null) { // 判断是否有加载回调
                mLoadListener.onLoad();
            }
        }
    }

    /**
     * 是否可以加载更多
     *
     * @return boolean true可以 false不可以
     */
    private boolean canLoadMore() {
        /*
        * ListView 滑动到底部
        * 当前正在执行上拉操作
        * 当前没有在加载数据
        * */
        return isListViewBottom() && isPullUp() && !mLoading;
    }

    /**
     * 判断ListView是否滑动到了底部
     *
     * @return boolean true滑动到了底部 false没有滑动到底部
     */
    private boolean isListViewBottom() {
        /*
        * 判断条件
        * 1. ListView 有子view
        * 2. 当前可以看见的最后一个子view的位置是最后一个子view
        * 3. 当前可以看见的最后一个子view的底部位置与ListView可显示内容区域高度相等
        *
        * 第二步获取ListView子view个数需要减一索引从0开始
        * 第三步的操作主要是解决防止在第二步最后一个子view还没有完全显示条件已经满足
        * */
        return mListView.getChildCount() > 0 &&
                mListView.getLastVisiblePosition() == (mListView.getCount() - 1) &&
                mListView.getChildAt(mListView.getChildCount() - 2).getBottom()
                        <= (mListView.getHeight() - mListView.getPaddingBottom());
    }

    /**
     * 是否是上拉操作
     *
     * @return boolean true是上拉操作 false不是上拉操作
     */
    private boolean isPullUp() {
        return (downY - moveY) >= mTouchSlop;
    }

    /**
     * 设置加载监听
     *
     * @param loadListener OnLoadListener
     */
    public void setOnLoadListener(@NonNull OnLoadListener loadListener) {
        mLoadListener = loadListener;
    }

    /**
     * 设置加载完成
     */
    public void setLoadingComplete() {
        mLoading = false; // 修改加载标记
        root.setVisibility(GONE); // 隐藏加载布局
    }

    /**
     * 设置加载布局
     *
     * @param footerView View
     */
    private void setLoadingView(View footerView) {
        if (footerView == null ||
                mListView.getFooterViewsCount() != 0) {
            return;
        }
        mListView.addFooterView(footerView);
    }

    /**
     * 设置刷新监听
     *
     * @param refreshDealListener OnRefreshDealListener
     */
    public void setOnRefreshDealListener(@NonNull OnRefreshDealListener refreshDealListener) {
        mRefreshDealListener = refreshDealListener;
    }

    @Override
    public void setOnRefreshListener(OnRefreshListener listener) {
        if (refreshListenerFlag) {
            super.setOnRefreshListener(listener);
            refreshListenerFlag = false;
        } else {
            throw new ErrorMethodException("请使用setOnRefreshDealListener方法");
        }
    }

    /**
     * 开始刷新
     *
     * @param handListener OnRefreshByHandListener
     */
    public void startRefresh(@NonNull OnRefreshByHandListener handListener) {
        if (mLoading) {
            handListener.onLoading();
        } else if (isRefreshing()) {
            handListener.onRefreshing();
        } else {
            setRefreshing(true);
            handListener.onRefresh();
        }
    }

    /**
     * 是否加载数据
     *
     * @return boolean true加载数据 false没有加载数据
     */
    public boolean isLoading() {
        return mLoading;
    }

    /**
     * 设置刷新完成
     */
    public void setRefreshingComplete() {
        setRefreshing(false);
    }

    public interface OnLoadListener {
        /**
         * 正在刷新
         */
        void onRefreshing();

        /**
         * 加载
         */
        void onLoad();
    }

    public interface OnRefreshDealListener {
        /**
         * 正在加载
         */
        void onLoading();

        /**
         * 刷新
         */
        void onRefresh();
    }

    public interface OnRefreshByHandListener {
        /**
         * 正在加载
         */
        void onLoading();

        /**
         * 正在刷新
         */
        void onRefreshing();

        /**
         * 刷新
         */
        void onRefresh();
    }
}
