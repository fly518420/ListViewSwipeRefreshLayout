package com.fly.swiperefreshlayout;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.fly.swiperefreshlayout.adapter.StringAdapter;
import com.fly.swiperefreshlayout.view.ListViewSwipeRefreshLayout;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private ListViewSwipeRefreshLayout listViewSwipeRefreshLayout;
    private StringAdapter adapter;
    private ArrayList<String> data;
    private RefreshHandler refreshHandler = new RefreshHandler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listViewSwipeRefreshLayout =
                (ListViewSwipeRefreshLayout) findViewById(R.id.id_list_refresh_layout);
        ListView listView = (ListView) findViewById(R.id.id_list_view);

        listViewSwipeRefreshLayout.setListView(listView,
                R.layout.swipe_load_layout, R.id.id_root_layout);

        // 设置刷新进度颜色变化
        listViewSwipeRefreshLayout.setColorSchemeColors(Color.BLUE, Color.GREEN, Color.RED);
        // 设置刷新进度的背景
        listViewSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(Color.argb(105, 22, 55, 66));
        // 设置刷新进度的大小
        listViewSwipeRefreshLayout.setSize(SwipeRefreshLayout.DEFAULT);

        data = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            data.add("item " + i);
        }
        adapter = new StringAdapter(this);
        adapter.setDataList(data);
        listView.setAdapter(adapter);

        listViewSwipeRefreshLayout.setOnRefreshDealListener(
                new ListViewSwipeRefreshLayout.OnRefreshDealListener() {
                    @Override
                    public void onLoading() {
                        Toast.makeText(MainActivity.this, "正在加载", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onRefresh() {
                        fetchData();
                    }
                });

        listViewSwipeRefreshLayout.setOnLoadListener(
                new ListViewSwipeRefreshLayout.OnLoadListener() {
                    @Override
                    public void onRefreshing() {
                        Toast.makeText(MainActivity.this, "正在刷新", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onLoad() {
                        fetchData();
                    }
                });

        findViewById(R.id.id_refresh_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listViewSwipeRefreshLayout.startRefresh(
                        new ListViewSwipeRefreshLayout.OnRefreshByHandListener() {
                            @Override
                            public void onLoading() {
                                Toast.makeText(MainActivity.this, "正在加载", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onRefreshing() {
                                Toast.makeText(MainActivity.this, "正在刷新", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onRefresh() {
                                fetchData();
                            }
                        });

            }

        });
    }

    public void fetchData() {
        final ArrayList<String> data = new ArrayList<>();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Random random = new Random();
                int count = random.nextInt(10);
                Message msg = refreshHandler.obtainMessage();
                if (count == 0 || count == 4) { // 没有数据
                    msg.what = 0;
                } else if (count == 2 || count == 7) { // 异常
                    msg.what = 1;
                } else { // 有数据
                    for (int i = 0; i < count; i++) {
                        data.add("new item " + i);
                    }
                    msg.what = 2;
                    msg.obj = data;
                }
                refreshHandler.sendMessage(msg);
            }
        }, 3000); // 模拟网络延时

    }

    private final class RefreshHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            boolean flag = false;
            if (listViewSwipeRefreshLayout.isRefreshing()) {
                flag = true;
                listViewSwipeRefreshLayout.setRefreshingComplete();
            }
            if (listViewSwipeRefreshLayout.isLoading()) {
                listViewSwipeRefreshLayout.setLoadingComplete();
            }
            switch (msg.what) {
                case 0:
                    Toast.makeText(MainActivity.this, "没有数据", Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    Toast.makeText(MainActivity.this, "异常", Toast.LENGTH_SHORT).show();
                    break;
                case 2:
                    ArrayList<String> newData = (ArrayList<String>) msg.obj;
                    if (flag) {
                        data.addAll(0, newData);
                    } else {
                        data.addAll(newData);
                    }
                    adapter.notifyDataSetChanged();
                    Toast.makeText(MainActivity.this, "数据 " + newData.size(), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }

}
