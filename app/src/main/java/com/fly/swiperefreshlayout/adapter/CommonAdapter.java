package com.fly.swiperefreshlayout.adapter;

import android.content.Context;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created with Android Studio.
 * Package_Name：com.fly.project.adapter
 * Project_Name：ChatDemo1
 * User：郭鹏飞
 * Date：2015/7/17
 * Email：love518420@foxmail.com
 * Description：公共的adapter
 */
public abstract class CommonAdapter<T> extends BaseAdapter {

    protected Context mContext;

    private ArrayList<T> dataList;

    public CommonAdapter(Context context) {
        mContext = context;
        dataList = new ArrayList<>();
    }

    /**
     * 设置数据
     *
     * @param dataList 数据源
     */
    public void setDataList(ArrayList<T> dataList) {
        this.dataList = dataList;
    }

    /**
     * 在指定位置插入元素
     *
     * @param t     元素
     * @param index 位置
     */
    public void insertDataList(T t, int index) {
        if (t == null)
            throw new NullPointerException("需要插入数据为空");
        if (index < 0 || index > dataList.size())
            throw new IllegalArgumentException("index is error");
        dataList.add(index, t);
    }

    /**
     * 在数据集合头部插入元素
     *
     * @param t 元素
     */
    public void insertDataListFrist(T t) {
        insertDataList(t, 0);
    }

    /**
     * 在数据集合尾部插入元素
     *
     * @param t 元素
     */
    public void insertDataListLast(T t) {
        insertDataList(t, dataList.size());
    }

    /**
     * 获取数据集合
     *
     * @return ArrayList
     */
    public ArrayList<T> getDataList() {
        return dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
