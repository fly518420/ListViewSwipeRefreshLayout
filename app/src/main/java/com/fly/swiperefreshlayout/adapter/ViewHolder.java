package com.fly.swiperefreshlayout.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with Android Studio.
 * Package_Name：com.fly.project.adapter
 * Project_Name：ChatDemo1
 * User：郭鹏飞
 * Date：2015/7/17
 * Email：love518420@foxmail.com
 * Description：公共的ViewHold - 避免每一个为每一个需要ViewHolder 都去定义一个
 * <p>
 * 采用了一种特殊的单例模式
 * 每一个Adapter 所持有的 ViewHolder 针对与该 Adapter 每一种View 都是单例的
 * </p>
 */
public class ViewHolder {

    // 记录当前布局id
    private static int convertId = -1;

    // 当前的布局
    private View convertView;

    // 已view的id作为key view作为value 存放在一个map集合
    private Map<Integer, View> childViewMap;

    private ViewHolder(Context context, int resId, ViewGroup parent) {
        if (context == null) {
            throw new NullPointerException("context is null");
        }
        LayoutInflater inflater = LayoutInflater.from(context);
        if (parent == null)
            convertView = inflater.inflate(resId, null);
        else
            convertView = inflater.inflate(resId, parent, false);

        // 实例化 集合
        childViewMap = new HashMap<>();

        convertView.setTag(this);
    }

    /**
     * 创建ViewHolder实例
     *
     * @param context     Context
     * @param convertView 布局
     * @param resId       布局id
     * @param parent      父布局
     * @return ViewHoler
     */
    public static ViewHolder createViewHolder(Context context,
                                              View convertView, int resId, ViewGroup parent) {
        if (convertView == null || convertId != resId) {
            convertId = resId;
            return new ViewHolder(context, resId, parent);
        }
        return (ViewHolder) convertView.getTag();

    }

    /**
     * 获取子View
     *
     * @param childResId 子View id
     * @return View
     */
    public synchronized View getChildView(int childResId) {
        // 先从集合中取
        View view = childViewMap.get(childResId);

        if (view == null) { // 集合中不存在该数据
            // 通过findViewById实例化
            view = convertView.findViewById(childResId);
            // 把该view存放到集合中
            childViewMap.put(childResId, view);
        }
        return view;
    }

    /**
     * 获取布局
     *
     * @return View
     */
    public View getConvertView() {
        return convertView;
    }
}
