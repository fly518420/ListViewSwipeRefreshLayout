package com.fly.swiperefreshlayout.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fly.swiperefreshlayout.R;

/**
 * Created with Android Studio.
 * Package_Name：com.fly.project.adapter
 * Project_Name：ChatDemo1
 * User：郭鹏飞
 * Date：2015/7/17
 * Email：love518420@foxmail.com
 * Description：
 */
public class StringAdapter extends CommonAdapter<String> {

    public StringAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = ViewHolder.createViewHolder(mContext,
                convertView, R.layout.string_item_layout, parent);

        TextView value = (TextView) holder.getChildView(R.id.value);
        value.setText(getDataList().get(position));

        return holder.getConvertView();
    }
}
