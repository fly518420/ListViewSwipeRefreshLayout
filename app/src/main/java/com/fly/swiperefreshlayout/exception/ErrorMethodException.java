package com.fly.swiperefreshlayout.exception;

/**
 * Created with Android Studio.
 * Package_Name：com.fly.glide
 * Project_Name：GlideDemo
 * User：郭鹏飞
 * Date：2015/12/25
 * Email：love518420@foxmail.com
 * Description：
 */
public class ErrorMethodException extends RuntimeException {

    private static String msg = "方法调用错误";

    public ErrorMethodException(String detailMessage) {
        super(detailMessage);
        msg = detailMessage;
    }

    @Override
    public String getMessage() {
        return msg;
    }
}
